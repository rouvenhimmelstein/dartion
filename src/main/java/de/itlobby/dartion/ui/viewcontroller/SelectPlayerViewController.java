package de.itlobby.dartion.ui.viewcontroller;

import de.itlobby.dartion.service.ConfigService;
import de.itlobby.dartion.service.MatchService;
import de.itlobby.dartion.ui.framework.ServiceLocator;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;

public class SelectPlayerViewController extends ViewController {
    public Button btnOnePlayer;
    public Button btnTwoPlayer;
    public Button btnThreePlayer;
    public Button btnFourPlayer;
    public CheckBox chkFixScaling;

    private MatchService matchService = ServiceLocator.get(MatchService.class);

    @Override
    public void init() {
        btnOnePlayer.setOnAction(actionEvent -> matchService.playerSelected(1));
        btnTwoPlayer.setOnAction(actionEvent -> matchService.playerSelected(2));
        btnThreePlayer.setOnAction(actionEvent -> matchService.playerSelected(3));
        btnFourPlayer.setOnAction(actionEvent -> matchService.playerSelected(4));

        chkFixScaling.setSelected(ConfigService.get().isFixScaling());
        chkFixScaling.selectedProperty().addListener((observableValue, oldValue, newValue) -> onFixScalingChanged(newValue));
    }

    private void onFixScalingChanged(Boolean fixScaling) {
        ConfigService.get().setFixScaling(fixScaling);
        ConfigService.save();
    }
}
