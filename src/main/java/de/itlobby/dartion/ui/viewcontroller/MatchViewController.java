package de.itlobby.dartion.ui.viewcontroller;

import de.itlobby.dartion.Main;
import de.itlobby.dartion.domain.Player;
import de.itlobby.dartion.domain.match.Match;
import de.itlobby.dartion.service.MatchService;
import de.itlobby.dartion.ui.component.PlayerPanelComponent;
import de.itlobby.dartion.ui.framework.ServiceLocator;
import de.itlobby.dartion.ui.listener.FocusNextPlayerListener;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

import java.util.ArrayList;

public class MatchViewController extends ViewController implements FocusNextPlayerListener {
    public Button btnEnd;
    public Button btnRestart;
    public Button btnEndApp;
    public FlowPane playerContainer;
    public AnchorPane baseLayout;

    private MatchService matchService = ServiceLocator.get(MatchService.class);

    @Override
    public void init() {
        btnEnd.setOnAction(actionEvent -> matchService.end());
        btnRestart.setOnAction(actionEvent -> matchService.restart());
        btnEndApp.setOnAction(actionEvent -> Main.exit());

        playerContainer.prefWidthProperty().bind(baseLayout.widthProperty());
        playerContainer.prefHeightProperty().bind(baseLayout.heightProperty().subtract(100));
    }

    public void buildMatchView(Match match) {
        playerContainer.getChildren().clear();
        match.setUpdateStatsListeners(new ArrayList<>());

        for (Player player : match.getPlayerList()) {
            PlayerPanelComponent playerPanelComponent = new PlayerPanelComponent(player, match);
            playerContainer.getChildren().add(playerPanelComponent);
            match.getUpdateStatsListeners().add(playerPanelComponent);
            match.setFocusNextPlayerListener(this);
        }
    }

    public void focusFirstPlayer() {
        playerContainer.getChildren().get(0).requestFocus();
    }

    @Override
    public void focusNextPlayer() {
        int currentFocusedIndex = playerContainer.getChildren().stream()
                .map(x -> (PlayerPanelComponent) x)
                .flatMap(x -> x.getChildren().stream())
                .filter(Node::isFocused)
                .map(x -> playerContainer.getChildren().indexOf(x.getParent()))
                .findFirst()
                .orElse(0);

        int nextFocus = (currentFocusedIndex + 1) % playerContainer.getChildren().size();
        playerContainer.getChildren().get(nextFocus).requestFocus();
    }

}
