package de.itlobby.dartion.ui.viewcontroller;

import de.itlobby.dartion.domain.FinishType;
import de.itlobby.dartion.domain.MatchType;
import de.itlobby.dartion.service.MatchService;
import de.itlobby.dartion.ui.framework.ServiceLocator;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.util.StringConverter;

import java.util.List;

public class SelectMatchTypeViewController extends ViewController {
    public ComboBox<FinishType> cmbFinishRules;
    public TextField txtPlayerName1;
    public TextField txtPlayerName2;
    public TextField txtPlayerName3;
    public TextField txtPlayerName4;
    public FlowPane matchTypePane;

    private MatchService matchService = ServiceLocator.get(MatchService.class);

    @Override
    public void init() {
        matchTypePane.getChildren().clear();
        for (MatchType matchType : MatchType.values()) {
            Button btn = new Button();
            btn.getStyleClass().add("mode-selection-button");
            btn.setText(matchType.getName());
            btn.setOnAction(actionEvent -> matchService.matchTypeSelected(matchType,
                    cmbFinishRules.getSelectionModel().getSelectedItem(),
                    List.of(txtPlayerName1.getText(),
                            txtPlayerName2.getText(),
                            txtPlayerName3.getText(),
                            txtPlayerName4.getText())
                    )
            );
            matchTypePane.getChildren().add(btn);
        }

        hidePlayerFields();

        fillComboBox();
    }

    private void fillComboBox() {
        cmbFinishRules.getItems().clear();
        cmbFinishRules.getItems().addAll(FinishType.values());
        cmbFinishRules.getSelectionModel().select(0);
        cmbFinishRules.setConverter(new StringConverter<>() {
            @Override
            public String toString(FinishType finishType) {
                return finishType == null ? null : finishType.getDisplayValue();
            }

            @Override
            public FinishType fromString(String s) {
                return FinishType.ofDisplayValue(s);
            }
        });
    }

    private void hidePlayerFields() {
        int playerCount = matchService.getPlayerCount();
        if (playerCount == 1) {
            txtPlayerName1.setVisible(true);
            txtPlayerName2.setVisible(false);
            txtPlayerName3.setVisible(false);
            txtPlayerName4.setVisible(false);

            txtPlayerName1.setManaged(true);
            txtPlayerName2.setManaged(false);
            txtPlayerName3.setManaged(false);
            txtPlayerName4.setManaged(false);
        }
        if (playerCount == 2) {
            txtPlayerName1.setVisible(true);
            txtPlayerName2.setVisible(true);
            txtPlayerName3.setVisible(false);
            txtPlayerName4.setVisible(false);

            txtPlayerName1.setManaged(true);
            txtPlayerName2.setManaged(true);
            txtPlayerName3.setManaged(false);
            txtPlayerName4.setManaged(false);
        }
        if (playerCount == 3) {
            txtPlayerName1.setVisible(true);
            txtPlayerName2.setVisible(true);
            txtPlayerName3.setVisible(true);
            txtPlayerName4.setVisible(false);

            txtPlayerName1.setManaged(true);
            txtPlayerName2.setManaged(true);
            txtPlayerName3.setManaged(true);
            txtPlayerName4.setManaged(false);
        }
        if (playerCount == 4) {
            txtPlayerName1.setVisible(true);
            txtPlayerName2.setVisible(true);
            txtPlayerName3.setVisible(true);
            txtPlayerName4.setVisible(true);

            txtPlayerName1.setManaged(true);
            txtPlayerName2.setManaged(true);
            txtPlayerName3.setManaged(true);
            txtPlayerName4.setManaged(true);
        }
    }
}
