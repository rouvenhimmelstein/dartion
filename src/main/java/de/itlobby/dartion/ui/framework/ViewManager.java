package de.itlobby.dartion.ui.framework;

import de.itlobby.dartion.service.ConfigService;
import de.itlobby.dartion.ui.listener.KeyPressedListener;
import de.itlobby.dartion.ui.viewcontroller.ViewController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class ViewManager {
    private static ViewManager instance;
    private Stage primaryStage;
    private FXMLFactory fxmlFactory;

    private ViewManager() {
    }

    public static ViewManager get() {
        if (instance == null) {
            instance = new ViewManager();
        }

        return instance;
    }

    public void initialize() {
        fxmlFactory = new FXMLFactory();

        activateView(Views.SELECT_PLAYER_VIEW);

        primaryStage.setHeight(getVisualHeight());
        primaryStage.setWidth(getVisualWidth());
        primaryStage.setMaximized(true);
        primaryStage.getIcons().add(new Image("icon/icon.png"));
        primaryStage.show();
    }

    private double getVisualWidth() {
        return Screen.getPrimary().getVisualBounds().getWidth();
    }

    private double getVisualHeight() {
        return Screen.getPrimary().getVisualBounds().getHeight();
    }

    public void activateView(Views viewToLoad) {
        if (primaryStage != null) {
            Scene scene = fxmlFactory.getView(viewToLoad);
            primaryStage.setScene(scene);
            primaryStage.setTitle(viewToLoad.getTitle());
            fxmlFactory.getViewController(viewToLoad, viewToLoad.getClazz()).init();

            if (ConfigService.get().isFixScaling()) {
                primaryStage.setMaximized(false);
                primaryStage.setMaximized(true);
            }
        }
    }

    public <T extends ViewController> T getViewController(Views view) {
        return (T) (view.getClazz()).cast(fxmlFactory.getViewController(view, view.getClazz()));
    }

    public Parent createLayoutFromView(Views view) {
        return fxmlFactory.createLayoutFromView(view);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void registerKeyPressedListener(Views view, KeyCodeCombination keyCodeCombination, KeyPressedListener keyPressedListener) {
        this.fxmlFactory.getView(view).getAccelerators().put(
                keyCodeCombination,
                keyPressedListener::keyPressed
        );
    }

    public void showViewAsStage(Views viewToShow) {
        Scene scene = fxmlFactory.getView(viewToShow);

        Stage stage = new Stage();
        stage.getIcons().add(new Image("icon/icon.png"));
        stage.setScene(scene);
        stage.setTitle(viewToShow.getTitle());
        stage.show();
    }

    public void closeView(Views viewToClose) {
        Scene view = getView(viewToClose);
        Stage stage = (Stage) view.getWindow();
        stage.close();
    }

    private Scene getView(Views view) {
        return fxmlFactory.getView(view);
    }

    public void setPrimaryTitle(String title) {
        primaryStage.setTitle(title);
    }

    public void putViewController(Views key, ViewController value) {
        fxmlFactory.putViewController(key, value);
    }
}
