package de.itlobby.dartion.ui.framework;

import de.itlobby.dartion.ui.viewcontroller.MatchViewController;
import de.itlobby.dartion.ui.viewcontroller.SelectMatchTypeViewController;
import de.itlobby.dartion.ui.viewcontroller.SelectPlayerViewController;
import de.itlobby.dartion.ui.viewcontroller.ViewController;

public enum Views {
    SELECT_PLAYER_VIEW("fxml/SelectPlayerView.fxml", "Spieleranzahl auswählen", SelectPlayerViewController.class),
    SELECT_MATCH_TYPE_VIEW("fxml/SelectMatchTypeView.fxml", "Spielart auswählen", SelectMatchTypeViewController.class),
    MATCH_VIEW("fxml/MatchView.fxml", "Los geht's", MatchViewController.class);

    private final String path;
    private final Class<ViewController> clazz;
    private String title;

    Views(String path, String title, Class<?> clazz) {
        this.path = path;
        this.title = title;
        this.clazz = (Class<ViewController>) clazz;
    }

    public String getPath() {
        return path;
    }

    public String getTitle() {
        return title;
    }

    public Class<ViewController> getClazz() {
        return clazz;
    }
}
