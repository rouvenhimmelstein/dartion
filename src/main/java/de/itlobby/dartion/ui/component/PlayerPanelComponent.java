package de.itlobby.dartion.ui.component;

import de.itlobby.dartion.domain.Player;
import de.itlobby.dartion.domain.ThrowValue;
import de.itlobby.dartion.domain.match.Match;
import de.itlobby.dartion.ui.listener.UpdateStatsListener;
import de.itlobby.dartion.util.FinishCalculator;
import de.itlobby.dartion.util.SystemUtil;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import org.apache.logging.log4j.util.Strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class PlayerPanelComponent extends VBox implements UpdateStatsListener {
    private Player player;
    private Match match;
    private Label lblName;
    private Label lblPointsLeft;
    private Label lblThrowCount;
    private Label lblAvgPoints;
    private Label lblPossibleFinish;
    private TextField txtFirstThrow;
    private TextField txtSecThrow;
    private TextField txtThirdThrow;
    private Button btnAccept;
    private int pointsLeftForPlayer;

    public PlayerPanelComponent(Player player, Match match) {
        this.player = player;
        this.match = match;
        buildUI();
    }

    private void buildUI() {
        getStyleClass().addAll("player-panel");
        setSpacing(5);
        setPadding(new Insets(5));

        addEventFilter(KeyEvent.KEY_RELEASED, this::handleKeyInput);

        lblName = new Label(player.getName());
        lblPointsLeft = new Label();
        lblThrowCount = new Label();
        lblAvgPoints = new Label();
        lblPossibleFinish = new Label();

        txtFirstThrow = new TextField("0");
        txtSecThrow = new TextField("0");
        txtThirdThrow = new TextField("0");

        btnAccept = new Button("Bestätigen");
        btnAccept.setOnAction(actionEvent -> finishThrows());

        btnAccept.setFocusTraversable(false);

        getChildren().add(lblName);

        getChildren().add(lblPointsLeft);
        getChildren().add(lblThrowCount);
        getChildren().add(lblAvgPoints);
        getChildren().add(lblPossibleFinish);

        getChildren().add(txtFirstThrow);
        getChildren().add(txtSecThrow);
        getChildren().add(txtThirdThrow);

        getChildren().add(btnAccept);

        VBox.setMargin(btnAccept, new Insets(0, 0, 0, 7));

        btnAccept.prefWidthProperty().bind(widthProperty().subtract(20));

        txtFirstThrow.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) ->
                handlePointsFocused(newPropertyValue, getPointsLeftSubtractedBy(txtFirstThrow.getText()), 2));
        txtSecThrow.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) ->
                handlePointsFocused(newPropertyValue, getPointsLeftSubtractedBy(txtFirstThrow.getText(), txtSecThrow.getText()), 1));

        if (player.getNumber() % 2 == 0) {
            setStyle("-fx-background-color: rgba(114,0,10,0.25);");
        } else {
            setStyle("-fx-background-color: rgba(7,105,7,0.25);");
        }

        updatePlayerStats();
    }

    private int getPointsLeftSubtractedBy(String... values) {
        return pointsLeftForPlayer -
                Arrays.stream(values)
                        .map(ThrowValue::of)
                        .filter(Optional::isPresent)
                        .mapToInt(x -> x.get().getFinalValue())
                        .sum();
    }

    private void handlePointsFocused(Boolean newPropertyValue, int pointsLeftForPlayer, int dartsLeft) {
        if (!newPropertyValue) {
            reCalculateFinishValues(pointsLeftForPlayer, dartsLeft);
        } else {
            reCalculateFinishValues(pointsLeftForPlayer, dartsLeft + 1);
        }
    }

    private void handleKeyInput(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            finishThrows();
        } else if (event.getCode() == KeyCode.DOWN) {
            SystemUtil.sendKey(java.awt.event.KeyEvent.VK_TAB);
        } else if (event.getCode() == KeyCode.UP) {
            SystemUtil.sendKeys(java.awt.event.KeyEvent.VK_SHIFT, java.awt.event.KeyEvent.VK_TAB);
        }
    }

    private void finishThrows() {
        String first = txtFirstThrow.getText();
        String sec = txtSecThrow.getText();
        String third = txtThirdThrow.getText();

        if (Strings.isNotBlank(first) && Strings.isNotBlank(sec) && Strings.isNotBlank(third)) {
            Optional<ThrowValue> firstVal = ThrowValue.of(first);
            Optional<ThrowValue> secVal = ThrowValue.of(sec);
            Optional<ThrowValue> thirdVal = ThrowValue.of(third);

            if (firstVal.isPresent() && secVal.isPresent() && thirdVal.isPresent()) {
                List<ThrowValue> throwRound = new ArrayList<>();
                throwRound.add(firstVal.get());
                throwRound.add(secVal.get());
                throwRound.add(thirdVal.get());

                int roundPoints = throwRound.stream().mapToInt(ThrowValue::getFinalValue).sum();
                int pointsLeft = match.calculatePointsLeftForPlayer(player);
                int diffPoints = pointsLeft - roundPoints;
                boolean aboutToFinish = diffPoints == 0;
                boolean isFinishValid = true;
                if (aboutToFinish) {
                    ThrowValue lastThrow = getLastValidThrow(throwRound);
                    isFinishValid = match.getFinishType().getFinishMultiplier().contains(lastThrow.getMultiplier());
                }

                if ((aboutToFinish && !isFinishValid) || diffPoints < 0) {
                    System.out.println("bust");
                    throwRound.forEach(ThrowValue::reset);
                    SystemUtil.playSound("sound/bust.wav");
                } else if (aboutToFinish && isFinishValid) {
                    System.out.println("proper finish");
                    SystemUtil.playSound("sound/win.wav");
                }

                player.getMatchThrows().add(throwRound);

                txtFirstThrow.setText("0");
                txtSecThrow.setText("0");
                txtThirdThrow.setText("0");

                match.notifyStatsUpdate();
            }
        }

        match.getFocusNextPlayerListener().focusNextPlayer();
        SystemUtil.playSound("sound/confirm.wav");
    }

    private ThrowValue getLastValidThrow(List<ThrowValue> throwRound) {
        ThrowValue o = null;
        int i = throwRound.size() - 1;

        while (o == null && i >= 0) {
            ThrowValue throwValue = throwRound.get(i);
            if (throwValue.getFinalValue() > 0) {
                o = throwValue;
            }
            i--;
        }

        return o;
    }

    private void updatePlayerStats() {
        pointsLeftForPlayer = match.calculatePointsLeftForPlayer(player);
        int lastPoints = player.getPointsFromLastThrow();
        double avgPointsPerThrow = player.getAvgPoints();
        int place = match.calculatePlaceForPlayer(player);

        if (match.hasPlayerFinished(player)) {
            txtFirstThrow.setDisable(true);
            txtSecThrow.setDisable(true);
            txtThirdThrow.setDisable(true);
            btnAccept.setDisable(true);
            place = player.getFinishPlace();
        }

        lblName.setText(String.format("%s (#%s)", player.getName(), place));
        lblPointsLeft.setText(String.format("Punkte: %d (+%d)", pointsLeftForPlayer, lastPoints));
        lblThrowCount.setText(String.format("Runden: %d", player.getMatchThrows().size()));
        lblAvgPoints.setText(String.format("Ø: %.1f (%.1f)", avgPointsPerThrow * 3.f, avgPointsPerThrow));

        reCalculateFinishValues(pointsLeftForPlayer, 3);
    }

    private void reCalculateFinishValues(int pointsLeftForPlayer, int dartsLeft) {
        List<ThrowValue> finishValues =
                FinishCalculator.getFinishValues(pointsLeftForPlayer, dartsLeft, match.getFinishType());
        lblPossibleFinish.setText(String.format("Finish: %s", finishValues));
    }

    @Override
    public void requestFocus() {
        super.requestFocus();
        txtFirstThrow.requestFocus();
        txtFirstThrow.selectAll();
    }

    @Override
    public void triggerUpdatePlayerStats() {
        updatePlayerStats();
    }
}
