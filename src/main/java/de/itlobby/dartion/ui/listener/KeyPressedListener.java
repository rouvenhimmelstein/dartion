package de.itlobby.dartion.ui.listener;

@FunctionalInterface
public interface KeyPressedListener {
    void keyPressed();
}
