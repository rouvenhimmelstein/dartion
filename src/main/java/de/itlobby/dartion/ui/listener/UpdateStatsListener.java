package de.itlobby.dartion.ui.listener;

public interface UpdateStatsListener {
    void triggerUpdatePlayerStats();
}
