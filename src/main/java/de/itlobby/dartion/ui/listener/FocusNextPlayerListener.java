package de.itlobby.dartion.ui.listener;

public interface FocusNextPlayerListener {
    void focusNextPlayer();
}
