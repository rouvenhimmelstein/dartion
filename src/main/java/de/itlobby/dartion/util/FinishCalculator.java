package de.itlobby.dartion.util;

import de.itlobby.dartion.domain.FinishType;
import de.itlobby.dartion.domain.ThrowValue;
import de.itlobby.dartion.domain.match.ThrowMultiplier;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FinishCalculator {
    private static final List<ThrowValue> allThrowValues = buildAllThrowableValues();
    private static Map<Integer, List<ThrowValue>> finishTable;
    private static Map<Integer, List<ThrowValue>> finishTableTwoDarts;

    public static void main(String[] args) {
        System.out.println(getFinishValues(148 - 19, 2, FinishType.DOUBLE_OUT));
    }

    public static List<ThrowValue> getFinishValues(int pointsLeftForPlayer, int dartsLeft, FinishType finishType) {
        buildFinishTables(finishType);

        List<ThrowValue> finishValues = new ArrayList<>();

        if (pointsLeftForPlayer <= finishType.getHighestPossibleFinish()) {
            finishValues = calculateFinishValues(pointsLeftForPlayer, dartsLeft, finishType.getFinishMultiplier());
        }

        return finishValues;
    }

    private static List<ThrowValue> calculateFinishValues(int pointsLeftForPlayer, int dartsLeft, List<ThrowMultiplier> finishMultiplier) {
        List<ThrowValue> finishValues = new ArrayList<>();

        switch (dartsLeft) {
            case 3:
                finishValues = getFinishValuesFromTable(pointsLeftForPlayer,
                        3,
                        finishMultiplier,
                        finishTable
                );
                break;
            case 2:
                finishValues = getFinishValuesFromTable(pointsLeftForPlayer,
                        2,
                        finishMultiplier,
                        finishTableTwoDarts
                );
                break;
            case 1:
                finishValues = combineThrowValues(pointsLeftForPlayer, 1, finishMultiplier);
                break;
        }

        return finishValues;
    }

    private static List<ThrowValue> getFinishValuesFromTable(int pointsLeftForPlayer,
                                                             int dartsLeft,
                                                             List<ThrowMultiplier> finishMultiplier,
                                                             Map<Integer, List<ThrowValue>> finishTable) {
        List<ThrowValue> finishValues;

        finishValues = finishTable.getOrDefault(
                pointsLeftForPlayer,
                combineThrowValues(pointsLeftForPlayer, dartsLeft, finishMultiplier)
        );

        return finishValues;
    }

    private static void buildFinishTables(FinishType finishType) {
        if (finishTable == null) {
            finishTable = buildFinishTable(finishType, false);
        }
        if (finishTableTwoDarts == null) {
            finishTableTwoDarts = buildFinishTable(finishType, true);
        }
    }

    public static void resetTables() {
        finishTable = null;
        finishTableTwoDarts = null;
    }

    private static Map<Integer, List<ThrowValue>> buildFinishTable(FinishType finishType, boolean twoDartsLeft) {
        Map<Integer, List<ThrowValue>> finishMap = new HashMap<>();

        try {
            String finishTableFile = finishType.getFinishTablePath();
            if (finishTableFile != null) {
                if (twoDartsLeft) {
                    finishTableFile += "-two-darts";
                }
                URL finishPathURL = SystemUtil.getResourceURL(finishTableFile);
                File file = new File(finishPathURL.getFile());
                if (file.exists()) {
                    String fileContent = FileUtils.readFileToString(file, "UTF-8");
                    String[] lines = fileContent.split("\n");
                    Arrays.stream(lines)
                            .map(FinishCalculator::tableLineToEntry)
                            .forEach(x -> finishMap.put(x.getKey(), x.getValue()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return finishMap;
    }

    private static AbstractMap.Entry<Integer, List<ThrowValue>> tableLineToEntry(String line) {
        String[] lineSplit = line.split(",");
        Integer value = Integer.valueOf(lineSplit[0]);
        List<ThrowValue> throwValues = Arrays.stream(lineSplit)
                .skip(1)
                .map(ThrowValue::of)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        return new HashMap.SimpleEntry<>(value, throwValues);
    }

    private static List<ThrowValue> combineThrowValues(int leftPoints, final int dartsLeft, List<ThrowMultiplier> throwMultiplier) {
        List<ThrowValue> throwValues = new ArrayList<>();

        boolean match = false;

        for (ThrowMultiplier multiplier : throwMultiplier) {
            for (int i = 0; dartsLeft >= 1 && i < allThrowValues.size() && !match; i++) {
                ThrowValue x = allThrowValues.get(i);
                if (f(x, ThrowValue.ZERO, ThrowValue.ZERO, leftPoints, multiplier)) {
                    throwValues.add(x);
                    match = true;
                }

                for (int j = 0; dartsLeft >= 2 && j < allThrowValues.size() && !match; j++) {
                    ThrowValue y = allThrowValues.get(j);
                    if (f(x, y, ThrowValue.ZERO, leftPoints, multiplier)) {
                        throwValues.add(x);
                        throwValues.add(y);
                        match = true;
                    }

                    for (int k = 0; dartsLeft >= 3 && k < allThrowValues.size() && !match; k++) {
                        ThrowValue z = allThrowValues.get(k);
                        if (f(x, y, z, leftPoints, multiplier)) {
                            throwValues.add(x);
                            throwValues.add(y);
                            throwValues.add(z);
                            match = true;
                        }
                    }
                }
            }
        }

        Collections.reverse(throwValues);
        return throwValues;
    }

    private static boolean f(ThrowValue x, ThrowValue y, ThrowValue z, int leftPoints, ThrowMultiplier throwMultiplier) {
        return x.getMultiplier().equals(throwMultiplier) && x.getFinalValue() + y.getFinalValue() + z.getFinalValue() == leftPoints;
    }

    private static List<ThrowValue> buildAllThrowableValues() {
        List<ThrowValue> list = new ArrayList<>();

        IntStream.rangeClosed(1, 20)
                .mapToObj(FinishCalculator::buildTripleOfNumber)
                .forEach(list::addAll);

        list.addAll(buildDoubleOfNumber(25));

        list.sort((t1, t2) ->
                Integer.compare(t2.getFinalValue(), t1.getFinalValue())
        );

        return list;
    }

    private static List<ThrowValue> buildTripleOfNumber(int number) {
        List<ThrowValue> tripleList = new ArrayList<>(3);
        tripleList.add(ThrowValue.of(number, ThrowMultiplier.NONE));
        tripleList.add(ThrowValue.of(number, ThrowMultiplier.DOUBLE));
        tripleList.add(ThrowValue.of(number, ThrowMultiplier.TRIPLE));
        return tripleList;
    }

    private static List<ThrowValue> buildDoubleOfNumber(int number) {
        List<ThrowValue> doubleList = new ArrayList<>(2);
        doubleList.add(ThrowValue.of(number, ThrowMultiplier.NONE));
        doubleList.add(ThrowValue.of(number, ThrowMultiplier.DOUBLE));
        return doubleList;
    }
}
