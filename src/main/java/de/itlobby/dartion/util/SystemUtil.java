package de.itlobby.dartion.util;

import de.itlobby.dartion.domain.OperatingSystem;
import de.itlobby.dartion.domain.SoundPlayer;

import java.net.URL;

public class SystemUtil {
    private static SoundPlayer soundPlayer = new SoundPlayer();

    public static URL getResourceURL(String path) {
        return Thread.currentThread().getContextClassLoader().getResource(path);
    }

    public static OperatingSystem getOs() {
        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win")) {
            return OperatingSystem.WINDOWS;
        } else if (os.contains("mac")) {
            return OperatingSystem.MAC;
        } else if (os.contains("nix") || os.contains("nux") || os.contains("aix")) {
            return OperatingSystem.UNIX;
        } else if (os.contains("sunos")) {
            return OperatingSystem.SOLARIS;
        } else {
            return OperatingSystem.OTHER;
        }
    }

    public static void sendKey(int key) {
        try {
            java.awt.Robot r = new java.awt.Robot();
            r.keyPress(key);
            r.keyRelease(key);
        } catch (java.awt.AWTException e) {
            e.printStackTrace();
        }
    }

    public static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }

    public static void sendKeys(int... keys) {
        try {
            java.awt.Robot r = new java.awt.Robot();

            for (int key : keys) {
                r.keyPress(key);
            }

            for (int key : keys) {
                r.keyRelease(key);
            }
        } catch (java.awt.AWTException e) {
            e.printStackTrace();
        }
    }

    public static void playSound(String resourcePath) {
        soundPlayer.play(resourcePath);
    }
}
