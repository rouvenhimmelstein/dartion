package de.itlobby.dartion.service;

import de.itlobby.dartion.domain.FinishType;
import de.itlobby.dartion.domain.MatchType;
import de.itlobby.dartion.domain.Player;
import de.itlobby.dartion.domain.match.Match;
import de.itlobby.dartion.domain.match.Match301;
import de.itlobby.dartion.ui.framework.ViewManager;
import de.itlobby.dartion.ui.framework.Views;
import de.itlobby.dartion.ui.viewcontroller.MatchViewController;
import de.itlobby.dartion.util.FinishCalculator;
import org.apache.logging.log4j.util.Strings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MatchService {
    private Match match;
    private int playerCount;
    private MatchType matchType;
    private FinishType finishType;
    private List<String> playerNames;

    private ViewManager viewManager = ViewManager.get();

    public void reset() {
        FinishCalculator.resetTables();
        match = null;
    }

    public void playerSelected(int playerCount) {
        this.playerCount = playerCount;
        viewManager.activateView(Views.SELECT_MATCH_TYPE_VIEW);
    }

    public void matchTypeSelected(MatchType matchType, FinishType finishType, List<String> playerNames) {
        this.matchType = matchType;
        this.finishType = finishType;
        this.playerNames = playerNames;

        viewManager.activateView(Views.MATCH_VIEW);

        start();
    }

    private void start() {
        match = createMatch(matchType);
        match.setPlayerCount(playerCount);
        match.setMatchType(matchType);
        match.setPlayerList(new ArrayList<>());
        match.setFinishType(finishType);

        viewManager.setPrimaryTitle(String.format("%s mit %s Spielern mit %s", matchType.getName(), playerCount, finishType.getDisplayValue()));

        for (int i = 1; i <= playerCount; i++) {
            Player player = new Player(i);
            player.setName(getPlayerName(i, playerNames.get(i - 1)));

            match.getPlayerList().add(player);
        }

        MatchViewController viewController = viewManager.getViewController(Views.MATCH_VIEW);
        viewController.buildMatchView(match);
        viewController.focusFirstPlayer();
    }

    private String getPlayerName(int i, String givenName) {
        String playerName = "Spieler " + i;

        if (Strings.isNotBlank(givenName)) {
            playerName = givenName;
        }

        return playerName;
    }

    private Match createMatch(MatchType matchType) {
        try {
            return matchType.getMatchClass().getConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Match301();
    }

    public void end() {
        reset();
        viewManager.activateView(Views.SELECT_PLAYER_VIEW);
    }

    public void restart() {
        List<String> players = new ArrayList<>(playerNames);
        players = players.stream().filter(Strings::isNotBlank).limit(playerCount).collect(Collectors.toList());
        Collections.reverse(players);
        while (players.size() < 4) {
            players.add("");
        }
        playerNames = players;

        start();
    }

    public int getPlayerCount() {
        return playerCount;
    }
}
