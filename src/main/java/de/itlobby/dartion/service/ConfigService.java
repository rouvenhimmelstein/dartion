package de.itlobby.dartion.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.itlobby.dartion.domain.AppConfig;
import de.itlobby.dartion.ui.framework.ServiceLocator;

import java.io.File;
import java.io.IOException;

public class ConfigService {
    private static final File configFile = new File(System.getProperty("user.home") + "/.dartion", "config.json");
    private static AppConfig appConfig = null;

    private static ConfigService getInstance() {
        return ServiceLocator.get(ConfigService.class);
    }

    public static AppConfig get() {
        return getInstance().getAppConfig();
    }

    public static void save() {
        try {
            new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(configFile, appConfig);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private AppConfig getAppConfig() {
        AppConfig localAppConfig = null;

        try {
            if (!configFile.exists()) {
                localAppConfig = new AppConfig();
                save();
            } else {
                if (appConfig == null) {
                    localAppConfig = new ObjectMapper().readValue(configFile, AppConfig.class);
                } else {
                    localAppConfig = appConfig;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        appConfig = localAppConfig;
        return appConfig;
    }
}
