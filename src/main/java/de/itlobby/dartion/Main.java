package de.itlobby.dartion;

import de.itlobby.dartion.domain.OperatingSystem;
import de.itlobby.dartion.service.MatchService;
import de.itlobby.dartion.ui.framework.ServiceLocator;
import de.itlobby.dartion.ui.framework.ViewManager;
import de.itlobby.dartion.util.SystemUtil;
import javafx.application.Platform;
import javafx.stage.Stage;

public class Main {
    public static void main(String[] args) {
        Platform.startup(() -> {
            if (SystemUtil.getOs() == OperatingSystem.UNIX) {
                System.setProperty("prism.text", "t2k");
                System.setProperty("prism.lcdtext", "true");
            }

            ViewManager viewManager = ViewManager.get();
            viewManager.setPrimaryStage(new Stage());
            viewManager.initialize();

            ServiceLocator.get(MatchService.class).reset();
        });
    }

    public static void exit() {
        Platform.exit();
        System.exit(0);
    }
}
