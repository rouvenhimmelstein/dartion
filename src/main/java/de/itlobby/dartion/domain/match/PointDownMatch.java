package de.itlobby.dartion.domain.match;

import de.itlobby.dartion.domain.Player;

import java.util.ArrayList;
import java.util.SortedMap;
import java.util.TreeMap;

public abstract class PointDownMatch extends Match {
    public abstract int getPoints();

    public int calculatePointsLeftForPlayer(Player player) {
        int left = getPoints() - player.getTotalPointsThrown();
        return left < 0 ? 0 : left;
    }

    public int calculatePlaceForPlayer(Player player) {
        SortedMap<Integer, Player> pointMap = new TreeMap<>();
        playerList.stream().filter(x -> !x.isFinished()).forEach(p -> pointMap.put(p.getTotalPointsThrown(), p));
        int indexOf = new ArrayList<>(pointMap.keySet()).indexOf(player.getTotalPointsThrown());
        return Math.abs((indexOf) - pointMap.size()) + (finishedPlaceCounter - 1);
    }

    public boolean hasPlayerFinished(Player player) {
        if (!player.isFinished() && calculatePointsLeftForPlayer(player) <= 0) {
            player.setFinished(true);
            player.setFinishPlace(finishedPlaceCounter++);
        }

        return player.isFinished();
    }
}