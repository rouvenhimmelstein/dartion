package de.itlobby.dartion.domain.match;

import java.util.Arrays;

public enum ThrowMultiplier {
    NONE(1, ""),
    DOUBLE(2, "D"),
    TRIPLE(3, "T");

    private final String shortDisplayValue;
    private int value;

    ThrowMultiplier(int value, String shortDisplayValue) {
        this.value = value;
        this.shortDisplayValue = shortDisplayValue;
    }

    public static ThrowMultiplier of(int multi) {
        return Arrays.stream(values()).filter(x -> x.value == multi).findFirst().orElse(NONE);
    }

    public static ThrowMultiplier ofLetter(String multi) {
        return Arrays.stream(values()).filter(x -> x.shortDisplayValue.equals(multi)).findFirst().orElse(NONE);
    }

    public int getValue() {
        return value;
    }

    public String getDisplayValue() {
        return shortDisplayValue;
    }
}
