package de.itlobby.dartion.domain.match;

import de.itlobby.dartion.domain.FinishType;
import de.itlobby.dartion.domain.MatchType;
import de.itlobby.dartion.domain.Player;
import de.itlobby.dartion.ui.listener.FocusNextPlayerListener;
import de.itlobby.dartion.ui.listener.UpdateStatsListener;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public abstract class Match {
    protected List<Player> playerList;
    protected int finishedPlaceCounter = 1;

    private int playerCount;
    private FinishType finishType;
    private MatchType matchType;
    private List<UpdateStatsListener> updateStatsListeners;
    private FocusNextPlayerListener focusNextPlayerListener;
    private boolean matchEnded;

    public abstract int calculatePointsLeftForPlayer(Player player);

    public abstract int calculatePlaceForPlayer(Player player);

    public abstract boolean hasPlayerFinished(Player player);

    public void notifyStatsUpdate() {
        updateStatsListeners.forEach(UpdateStatsListener::triggerUpdatePlayerStats);
        checkForMatchEnd();
    }

    private void checkForMatchEnd() {
        List<Player> finishedPlayer = playerList.stream()
                .filter(this::hasPlayerFinished)
                .collect(Collectors.toList());
        if (finishedPlayer.size() == playerList.size()) {
            endMatch();
        }
    }

    private void endMatch() {
        matchEnded = true;
    }

    public FocusNextPlayerListener getFocusNextPlayerListener() {
        return focusNextPlayerListener;
    }

    public void setFocusNextPlayerListener(FocusNextPlayerListener focusNextPlayerListener) {
        this.focusNextPlayerListener = focusNextPlayerListener;
    }
}
