package de.itlobby.dartion.domain;

import de.itlobby.dartion.domain.match.Match;
import de.itlobby.dartion.domain.match.Match301;
import de.itlobby.dartion.domain.match.Match501;
import de.itlobby.dartion.domain.match.Match701;

public enum MatchType {
    T301(Match301.class, "301"),
    T501(Match501.class, "501"),
    T701(Match701.class, "701");

    private Class<? extends Match> matchClass;
    private String name;

    MatchType(Class<? extends Match> matchClass, String name) {
        this.matchClass = matchClass;
        this.name = name;
    }

    public Class<? extends Match> getMatchClass() {
        return matchClass;
    }

    public String getName() {
        return name;
    }
}
