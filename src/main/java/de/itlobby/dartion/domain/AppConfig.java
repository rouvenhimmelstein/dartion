package de.itlobby.dartion.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class AppConfig implements Serializable {
    private boolean fixScaling;
}
