package de.itlobby.dartion.domain;

public enum OperatingSystem {
    WINDOWS,
    MAC,
    UNIX,
    SOLARIS,
    OTHER
}
