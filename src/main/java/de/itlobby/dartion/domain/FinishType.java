package de.itlobby.dartion.domain;

import de.itlobby.dartion.domain.match.ThrowMultiplier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum FinishType {
    STRAIGHT_OUT("Straight Out", 180, null, ThrowMultiplier.NONE, ThrowMultiplier.DOUBLE, ThrowMultiplier.TRIPLE),
    DOUBLE_OUT("Double Out", 170, "finish-table/double-out-finish", ThrowMultiplier.DOUBLE),
    MASTER_OUT("Master Out", 180, "finish-table/master-out-finish", ThrowMultiplier.DOUBLE, ThrowMultiplier.TRIPLE);

    private final String displayValue;
    private final Integer highestPossibleFinish;
    private final String finishTablePath;
    private final List<ThrowMultiplier> finishMultiplier;

    FinishType(String displayValue, Integer highestPossibleFinish, String finishTablePath, ThrowMultiplier... finishMultiplier) {
        this.displayValue = displayValue;
        this.highestPossibleFinish = highestPossibleFinish;
        this.finishTablePath = finishTablePath;
        this.finishMultiplier = new ArrayList<>(Arrays.asList(finishMultiplier));
    }

    public static FinishType ofDisplayValue(String displayValue) {
        return Arrays.stream(values()).filter(x -> x.getDisplayValue().equals(displayValue)).findFirst().orElse(null);
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public List<ThrowMultiplier> getFinishMultiplier() {
        return finishMultiplier;
    }

    public String getFinishTablePath() {
        return finishTablePath;
    }

    public Integer getHighestPossibleFinish() {
        return highestPossibleFinish;
    }
}
