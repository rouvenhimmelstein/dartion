package de.itlobby.dartion.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class Player {
    private int number;
    private String name;
    private List<List<ThrowValue>> matchThrows = new ArrayList<>();
    private boolean finished;
    private int finishPlace;

    public Player(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<List<ThrowValue>> getMatchThrows() {
        return matchThrows;
    }

    public void setMatchThrows(List<List<ThrowValue>> matchThrows) {
        this.matchThrows = matchThrows;
    }

    public int getTotalPointsThrown() {
        return matchThrows.stream()
                .flatMap(Collection::stream)
                .flatMapToInt(x -> IntStream.of(x.getFinalValue()))
                .sum();
    }

    public int getPointsFromLastThrow() {
        return matchThrows.size() > 0 ? matchThrows.get(matchThrows.size() - 1).stream().mapToInt(ThrowValue::getFinalValue).sum() : 0;
    }

    public double getAvgPoints() {
        return matchThrows.stream()
                .flatMap(Collection::stream)
                .flatMapToDouble(x -> DoubleStream.of((double) x.getFinalValue()))
                .average().orElse(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return number == player.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getFinishPlace() {
        return finishPlace;
    }

    public void setFinishPlace(int finishPlace) {
        this.finishPlace = finishPlace;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

}
