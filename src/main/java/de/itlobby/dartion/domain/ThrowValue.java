package de.itlobby.dartion.domain;

import de.itlobby.dartion.domain.match.ThrowMultiplier;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.itlobby.dartion.util.SystemUtil.isInt;

public class ThrowValue {
    private static final Pattern letterPattern = Pattern.compile("((?<multi>[DT])?(?<value>[\\d|DB|SB|BULL]+))");
    public static ThrowValue ZERO = ThrowValue.of(0, ThrowMultiplier.NONE);
    private int singleValue = 0;
    private ThrowMultiplier multiplier = ThrowMultiplier.NONE;

    public static ThrowValue of(int value, ThrowMultiplier multiplier) {
        ThrowValue throwValue = new ThrowValue();
        throwValue.setSingleValue(value);
        throwValue.setMultiplier(multiplier);
        return throwValue;
    }

    public static Optional<ThrowValue> of(String stringValue) {
        ThrowValue throwValue = new ThrowValue();
        Matcher matcher = letterPattern.matcher(stringValue.trim());

        boolean match = false;
        while (matcher.find()) {
            match = true;

            String rawMulti = matcher.group("multi");
            ThrowMultiplier multi = rawMulti == null ? ThrowMultiplier.NONE : ThrowMultiplier.ofLetter(rawMulti);


            String sValue = matcher.group("value");
            int value = 0;
            if (isInt(sValue)) {
                value = Integer.parseInt(sValue);
            } else if (sValue.toLowerCase().equals("b")) {
                value = 25;
                multi = ThrowMultiplier.DOUBLE;
            } else if (sValue.toLowerCase().equals("sb")) {
                value = 25;
            } else if (sValue.toLowerCase().equals("bull")) {
                value = 25;
                multi = ThrowMultiplier.DOUBLE;
            }

            throwValue.setMultiplier(multi);
            throwValue.setSingleValue(value);
        }

        if (!match) {
            throwValue = null;
        }

        return Optional.ofNullable(throwValue);
    }

    public void reset() {
        singleValue = 0;
        multiplier = ThrowMultiplier.NONE;
    }

    public int getFinalValue() {
        return singleValue * multiplier.getValue();
    }

    public int getSingleValue() {
        return singleValue;
    }

    public void setSingleValue(int singleValue) {
        this.singleValue = singleValue;
    }

    public ThrowMultiplier getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(ThrowMultiplier multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThrowValue that = (ThrowValue) o;
        return singleValue == that.singleValue &&
                multiplier == that.multiplier;
    }

    @Override
    public int hashCode() {
        return Objects.hash(singleValue, multiplier);
    }

    @Override
    public String toString() {
        return String.format("%s%s", multiplier.getDisplayValue(), singleValue);
    }
}
