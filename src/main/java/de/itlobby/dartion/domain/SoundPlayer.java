package de.itlobby.dartion.domain;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.net.URISyntaxException;

import static de.itlobby.dartion.util.SystemUtil.getResourceURL;

public class SoundPlayer {
    @SuppressWarnings("FieldCanBeLocal")
    private MediaPlayer mediaPlayer = new MediaPlayer(loadMedia("sound/silent.wav"));

    public SoundPlayer() {
        mediaPlayer.play();
    }

    public void play(String resourcePath) {
        try {
            mediaPlayer = new MediaPlayer(loadMedia(resourcePath));
            mediaPlayer.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Media loadMedia(String resourcePath) {
        try {
            String mediaPath = getResourceURL(resourcePath).toURI().toString();
            return new Media(mediaPath);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return new Media("file:target/classes/sound/silent.wav");
    }
}
