# Base configuration
appbase = http://theupload.de/app/dartion/getdown/
code = dartion.jar
class = manifest
allow_offline = true

# Resources
resource = launcher-background.jpg
resource = icon.png

# UI configuration
ui.name = dartion
ui.background_image = launcher-background.jpg
ui.icon = icon.png
ui.progress_bar = EFEFEF
ui.progress_text = 0C0C0C
ui.status_text = 0C0C0C
ui.progress = 15, 400, 600, 30